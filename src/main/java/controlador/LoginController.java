package controlador;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import controladorbd.ConexionBD;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController implements Initializable {
	@FXML
	private Button btnEntrar;
	
	@FXML
	private TextField tfUser;
	
	@FXML
	private PasswordField tfPasswd;
	
	Connection con;
	
   	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			con = ConexionBD.getConexion();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			Platform.exit();
		}
	}
   	
   	@FXML
    private void accionEntrar() {
   		int id;
        String usr, pw, sql;
        ResultSet rs;
        Statement st;
        
        try {            
            usr = tfUser.getText();
            //id = Integer.parseInt(usr);
            pw = tfPasswd.getText();

            // Sin PreparedStatement  
            // Peligros: usuario ( or true ) o en contrasenya ( xxx' o '1 )
            // where id = 1 or true and passwd = '1234'
            // where id = 1 and passwd = '1234' or '1'
            sql = "SELECT * FROM empresa_ad.clientes where id = " + usr + " and passwd = '" + pw + "'";
            System.out.println("Sentencia final: " + sql);
            st = con.createStatement();
            rs = st.executeQuery(sql);

            // ----------------------------------------------
            // Con PreparedStatement
//            PreparedStatement pst;
//            id = Integer.parseInt(tfUser.getText());
//            pw = tfPasswd.getText();
//            sql = "SELECT * FROM clientes where id = ? and passwd = ?";
//            pst = con.prepareStatement(sql);
//            pst.setInt(1, id);
//            pst.setString(2, pw);
//            System.out.println(pst.toString());
//            rs = pst.executeQuery();
//            
            if (rs.first()) {
                System.out.println("Usuario encontrado -> PUEDES ENTRAR");             
                rs.beforeFirst();
                while (rs.next()) {
                    System.out.print(rs.getInt(1) + " - ");
                    System.out.println(rs.getString(2));
                }
            } else {
                System.out.println("usuario no encontrado -> NO ENTRAS");
            }

            
        } catch (SQLException ex) {
            System.out.println("Error SQL: " + ex.getMessage());
        }
   	}
}
